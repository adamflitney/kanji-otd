# kanji-otd

A simple project to show random kanji and reveal their meaning by clicking them.

I have now also added keyboard shortcuts, right arrow shows a new kanji, left arrow goes back through the previous kanji you have seen this session, and space reveals the meaning and more info button for the current kanji.

![](kanji-otd-screenshot.png)

A demo is running at: https://confident-mccarthy-bc21be.netlify.com/

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```
